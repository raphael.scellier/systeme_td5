#include <stdio.h>
#include <stdlib.h>
#include "temps.h"


int main()
{
    struct temps t1,t2,total;
    printf("Entrez le premier temps : \n");
    scanf("%d",&t1.heure);
    scanf("%d",&t1.minute);
    printf("Entrez le second temps : \n");
    scanf("%d",&t2.heure);
    scanf("%d",&t2.minute);
    total = addTime(t1,t2);
    printf("total des temps : %d:%d\n",total.heure,total.minute);
    printf("temps 1 : %d\n",timeToMinutes(t1));
    printf("temps 2 : %d\n",timeToMinutes(t2));
    printf("temps total : %d\n",timeToMinutes(total));
    exit(0);
}